#standard library packages
import os
from datetime import datetime, timedelta
from typing import List

#fastAPI related packages
from fastapi import APIRouter, Response, BackgroundTasks, Query, Depends, File, UploadFile, HTTPException
from sqlalchemy.orm import Session
from starlette.responses import FileResponse
from werkzeug.utils import secure_filename

#local packages
from ..utils import auth, crud, files, models, schemas
from ..utils.other import check_for_404
from ..utils.files import get_upload_folder
from ..utils.database import get_db

router = APIRouter(prefix="/node", tags=["Worker Nodes"])

allowed_node_statuses = {"online", "created", "maintenance", "expired"}

def check_maintenance(db_node):
    if db_node.status == "maintenance":
        raise HTTPException(status_code=400, detail="worker in maintenance mode")
    else:
        pass

@router.get("/")
def get_all_nodes(
        db: Session = Depends(get_db),
        token: str = Depends(auth.oauth2_scheme),
        status: str = None,
        skip: int = 0,
        limit: int = 100,
        ):
        """Return a list of all nodes registered with the server"""
        client = auth.get_current_client(db, "user", token)
        return crud.get_nodes(db, status, skip, limit)

@router.post("/")
def create_node(
        node: schemas.NodeCreate,
        db: Session = Depends(get_db),
        token: str = Depends(auth.oauth2_scheme),
        ):
    """Route for administrators to create new nodes with the server.
    Nodes won't have tasks assigned to them until they call the API and request them"""
    auth.get_current_client(db, "user", token)
    if crud.get_node_by_name(db, node.name) is not None:
        raise HTTPException(status_code=400, detail="Node Name already taken")
    if node.status not in allowed_node_statuses:
        raise HTTPException(status_code=400, detail="Status not allowed")
    expiration = (datetime.utcnow() + timedelta(minutes=30))
    db_node = crud.create_node(db, node, expiration)
    access_token_expires = timedelta(minutes=auth.ACCESS_TOKEN_EXPIRE_MINUTES)
    token = auth.create_access_token(
            data={"token_type":"node", "sub": node.name}, expires_delta=access_token_expires
            )
    return [db_node, token]

@router.get("/{id}")
def get_node(
        id: int,
        db: Session = Depends(get_db),
        token: str = Depends(auth.oauth2_scheme),
        ):
    """Returns a single node by its ID"""
    auth.get_current_client(db, "user", token)
    db_node = crud.get_node_by_id(db, id)
    check_for_404(db_node)
    return db_node


@router.put("/{id}")
def update_node(
        background_tasks: BackgroundTasks,
        id: int,
        node: schemas.NodeCreate,
        db: Session = Depends(get_db),
        token: str = Depends(auth.oauth2_scheme),
        ):
    """Route for administrators to update a node's status"""
    #todo limit possible node statuses
    auth.get_current_client(db, "user", token)
    if node.status not in allowed_node_statuses:
        raise HTTPException(status_code=400, detail="Status not allowed")
    if node.status == "maintenance":
       background_tasks.add_task(crud.deallocate_tasks, db, id) 
    db_node = crud.update_node(db, id, node)
    check_for_404(db_node)
    return db_node

@router.get("/files/")
def get_file_list(
        db: Session = Depends(get_db),
        token: str = Depends(auth.oauth2_scheme),
        ):
    """Route that returns a list of files for unfinished jobs.
    Nodes will then compare the results of this list with what's locally stored and then download them."""
    node = auth.get_current_client(db, "node", token)
    check_maintenance(node)
    return crud.get_active_job_files(db)

@router.get("/files/{id}")
def get_job_file(
        id: int,
        db: Session = Depends(get_db),
        token: str = Depends(auth.oauth2_scheme),
        upload_folder = Depends(get_upload_folder),
        ):
    """ID refers to a file ID returned from /node/files"""
    node = auth.get_current_client(db, "node", token)
    check_maintenance(node)
    file = crud.get_file_entry(db, id)
    check_for_404(file)
    directory = files.get_directory_string(str(file.user), str(file.name), upload_folder)
    return FileResponse(directory, media_type='octet')

@router.get("/token")
def get_node_token(
        db: Session = Depends(get_db),
        token: str = Depends(auth.oauth2_scheme),
        ):
    """This route is for nodes to obtain new tokens"""
    node = auth.get_current_client(db, "node", token)
    access_token_expires = timedelta(minutes=auth.ACCESS_TOKEN_EXPIRE_MINUTES)
    token = auth.create_access_token(
            data={"token_type":"node", "sub": node.name}, expires_delta=access_token_expires
            )
    return token

@router.get("/task/")
def get_node_task(
        file: List[int] = Query(None),
        db: Session = Depends(get_db),
        token: str = Depends(auth.oauth2_scheme),
        status: str = None,
        ):
    """Route for Nodes to request a new task based on a list of files they've downloaded"""
    node = auth.get_current_client(db, "node", token)
    check_maintenance(node)
    if file == None:
        raise HTTPException(
                status_code=400,
                detail='No tasks available due to no files in request'
                )
    db_task = crud.get_task_by_files(db, file, status)
    check_for_404(db_task)
    db_task.node = int(node.id)
    db_task.status = 'started'
    db_task.start_time = datetime.utcnow()
    db.commit()
    db.refresh(db_task)
    return db_task

@router.put("/task/{task_id}")
def update_task(
        background_tasks: BackgroundTasks,
        task_id: int,
        task: schemas.TaskUpdate,
        db: Session = Depends(get_db),
        token: str = Depends(auth.oauth2_scheme),
        upload_folder = Depends(get_upload_folder),
        ):
    """Route for nodes to update the status of a task"""
    #todo limit possible task statuses
    node = auth.get_current_client(db, "node", token)
    check_maintenance(node)
    db_task = crud.get_task(db, task_id)
    check_for_404(db_task)
    db_task.status = task.status
    if task.status == 'complete':
        db_task.end_time = datetime.utcnow()
        if crud.get_first_incomplete_task_by_job(db, db_task.job) == None:
            db_job = crud.get_job(db, db_task.job)
            db_job.status = 'complete'
            db_job.end_time = db_task.end_time
            background_tasks.add_task(files.compress_finished_job, db_job, upload_folder)
    db.commit()
    return db_task

@router.post("/task/{task_id}/log", status_code=200)
def submit_logs(
        task_id: int,
        task_log: schemas.TaskLogCreate,
        db: Session = Depends(get_db),
        token: str = Depends(auth.oauth2_scheme),
        ):
    node = auth.get_current_client(db, "node", token)
    db_task = crud.get_task(db, task_id)
    check_for_404(db_task)
    log = task_log.log
    submit_time = datetime.utcnow()
    db_task_log = crud.create_task_log(db, task_id, log, node.id, submit_time)
    return {"detail": "success"}

@router.post("/result/{id}")
def upload_result(
        id: int,
        file: UploadFile = File(...),
        db: Session = Depends(get_db),
        token: str = Depends(auth.oauth2_scheme),
        upload_folder = Depends(get_upload_folder),
        ):
    """ID in this route refers to a task id"""
    node = auth.get_current_client(db, "node", token)
    check_maintenance(node)
    task = crud.get_task(db, id)
    check_for_404(task)
    if int(node.id) != int(task.node):
        raise HTTPException(
                status_code=HTTP_403_FORBIDDEN,
                detail="no access to resouce"
                )
    job = crud.get_job(db, task.job)
    file.filename = secure_filename(file.filename)
    result = crud.create_result(db, job.id, task.id, file.filename)
    directory = os.path.join(upload_folder, str(job.user), str(job.id), file.filename)
    files.save_file(directory, file.file)
    task.status = 'complete'
    db.commit()
    return result

