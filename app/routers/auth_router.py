#standard library packages
import os
import shutil
import random
from datetime import datetime, timedelta
from typing import List

#fastAPI related packages
from fastapi import (APIRouter, Depends,
                    File, UploadFile,
                    HTTPException, Query, Header,
                    Request)
from fastapi.security import (OAuth2PasswordBearer,
                              OAuth2PasswordRequestForm)

from fastapi.responses import RedirectResponse

from sqlalchemy.orm import Session
from starlette.responses import FileResponse
from werkzeug.utils import secure_filename

#local packages
from ..utils import auth, crud, files, models, schemas
from ..utils.other import check_for_404
from ..utils.files import get_upload_folder
from ..utils.database import get_db

router = APIRouter(prefix="/auth", tags=["Authentication"])

@router.post("/create", response_model=schemas.User)
def create_user(new_user: schemas.UserCreate,
        db: Session = Depends(get_db),
        token: str = Header(None)):
    """Route for adding users to the server"""
    user_check = crud.get_first_user(db)
    if user_check != None:
        user = auth.get_current_client(db, "user", token)
        if user.admin == False:
           raise HTTPException(
                   status_code=HTTP_401_UNAUTHORIZED,
                   detail="Insufficient Access",
                   )
    new_user.username = str.lower(new_user.username)
    new_user.email = str.lower(new_user.email)
    if crud.get_user_by_username(db, new_user.username) is not None:
        raise HTTPException(status_code=400, detail="username already in use")
    if crud.get_user_by_email(db, new_user.email) is not None:
        raise HTTPException(status_code=400, detail="email already in use")
    new_user.password = auth.get_password_hash(new_user.password)
    if user_check == None:
        new_user.admin = True
    #figure out admin and user creation time with new schemas
    new_user = crud.create_user(db, new_user)
    return new_user

@router.post("/test", response_model=schemas.User)
def this_is_a_test(db: Session = Depends(get_db), token: str = Depends(auth.oauth2_scheme)):
    """Test URL for verifying a user's token (useful for debugging and testing)"""
    user = auth.get_current_client(db,"user", token)
    return user

@router.post("/token")
def obtain_oauth_token(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    """OAuth token issuer"""
    #todo reduce jank of user tokens/node tokens
    user = auth.authenticate_user_password(db, str.lower(form_data.username), form_data.password)
    if not user:
        raise HTTPException(
                status_code=401,
                detail="Incorrect username or password",
                headers={"WWW-Authentice":"Bearer"}
            )
    access_token_expires = timedelta(minutes=auth.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = auth.create_access_token(
            data={"token_type": "user", "sub": user.username, "admin": user.admin}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}

@router.post("/node/{id}")
def get_token_for_node(
        id: int,
        db: Session = Depends(get_db),
        token: str = Depends(auth.oauth2_scheme),
        ):
    """This route is for administrators to obtain a new token for nodes in the farm"""
    auth.get_current_client(db, "user", token)
    node = crud.get_node_by_id(db, id)
    node_token = auth.create_access_token(data = {"token_type": "node", "sub": node.name},
            expires_delta=timedelta(days=2))
    return [node, node_token]

