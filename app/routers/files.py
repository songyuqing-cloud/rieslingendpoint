#standard library packages
import os

#fastAPI related packages
from fastapi import APIRouter, Depends, File, UploadFile, HTTPException
from sqlalchemy.orm import Session
from starlette.responses import FileResponse
from werkzeug.utils import secure_filename

#local packages
from ..utils import auth, crud, files, models, schemas
from ..utils.other import check_for_404
from ..utils.files import get_upload_folder
from ..utils.database import get_db

router = APIRouter(prefix="/files",
                    tags=["Files"],)

@router.get('/')
def get_files(
        db: Session = Depends(get_db), 
        token: str = Depends(auth.oauth2_scheme),
        skip: int = 0,
        limit: int = 100,
        ):
    """Route for a user to get a list of all the files uploaded to the server"""
    user = auth.get_current_client(db, 'user', token)
    return crud.get_files_by_user(db, user.id, skip, limit)

@router.post("/")
async def create_file(
        db: Session = Depends(get_db),
        file: UploadFile = File(...),
        token: str = Depends(auth.oauth2_scheme),
        upload_folder = Depends(get_upload_folder),
        ):
    """Route for users to upload blender files to the server"""
    #todo implement storage limits
    user = auth.get_current_client(db, "user", token)
    file.filename = secure_filename(file.filename)
    try:
        os.mkdir(os.path.join(upload_folder, str(user.id)))
    except(FileExistsError):
        pass
    upload_directory = os.path.join(upload_folder, str(user.id), file.filename)
    files.save_file(upload_directory, file.file)
    size = files.get_file_size(upload_directory)
    db_file = schemas.FileCreate(name=file.filename, size=size, user=user.id)
    crud.create_file_entry(db, db_file)
    return {"filename": file.filename}

@router.get("/{user_id}/{file_id}")
async def retrieve_stored_blender_file(
        user_id: int,
        file_id: int = None,
        token: str = Depends(auth.oauth2_scheme),
        upload_folder = Depends(get_upload_folder),
        db: Session = Depends(get_db),
        ):
    """Allow users to download files they've uploaded"""
    user = auth.get_current_client(db, "user", token)
    if user.id != user_id:
        raise HTTPException(status_code=401, detail="No Access to Requested Resource")
    db_file = crud.get_file_entry(db, file_id)
    check_for_404(db_file)
    directory = files.get_directory_string(str(db_file.user), str(db_file.name), upload_folder)
    return FileResponse(directory, media_type='octet', filename=db_file.name)

@router.get("/{job_id}/result/")
async def retrieve_job_result(
        job_id: int,
        token: str = Depends(auth.oauth2_scheme),
        upload_folder = Depends(get_upload_folder),
        db: Session = Depends(get_db),
        ):
    """Route for users to download the final results of their jobs running"""
    user = auth.get_current_client(db, "user", token)
    db_job = crud.get_job(db, job_id)
    check_for_404(db_job)
    if user.id != db_job.user:
        raise HTTPException(status_code=401, detail="No Access to Requested Resource")
    if files.check_for_compressed_file(db_job, upload_folder) == False:
        raise HTTPException(status_code=404, detail="job not finished yet, or results not compressed yet")
    directory_string = files.get_compressed_file_directory_string(db_job, upload_folder)
    return FileResponse(directory_string, media_type='octet', filename=files.create_output_file_name(db_job))

@router.get("/rieslingtest")
async def get_benchmark_file(
        upload_folder = Depends(get_upload_folder),
        ):
    """A route for obtaining the benchmarking blender file"""
    return FileResponse(os.path.join(upload_folder, "rieslingtest.blend"), media_type='octet', filename='rieslingtest.blend')

@router.get("/smoltest")
async def get_small_file(
        upload_folder = Depends(get_upload_folder),
        ):
    """A route for obtaining the much smaller test file"""
    return FileResponse(os.path.join(upload_folder, "smol.blend"), media_type='octet', filename='rieslingtest.blend')

