from fastapi import APIRouter

router = APIRouter(prefix="/status", tags=["status"])

@router.get("/")
def return_status():
    """This route will be used in future version to inform users if a server is undergoing maintenance"""
    return {"status": "online"}
