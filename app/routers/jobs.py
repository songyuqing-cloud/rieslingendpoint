#standard library packages
import os

#fastAPI related packages
from fastapi import APIRouter, Depends, File, UploadFile, HTTPException
from sqlalchemy.orm import Session
from starlette.responses import FileResponse
from werkzeug.utils import secure_filename

#local packages
from ..utils import auth, crud, files, models, schemas
from ..utils.other import check_for_404
from ..utils.files import get_upload_folder
from ..utils.database import get_db

router = APIRouter(prefix="/job", tags=["Jobs"])
allowed_render_engines = {"BLENDER_EEVEE", "BLENDER_WORKBENCH", "CYCLES"}

@router.post("/render", response_model=schemas.Job)
def create_job(
        job: schemas.JobBase,
        db: Session = Depends(get_db),
        token: str = Depends(auth.oauth2_scheme),
        upload_folder = Depends(get_upload_folder),
        ):
    """The main function that basically started it all, this is where a user will create new render jobs"""
    user = auth.get_current_client(db,"user", token)
    if job.render_engine not in allowed_render_engines:
        raise HTTPException(status_code=422, detail="Render engine not supported, or not entered correctly (Use ALL_CAPS)")
    job = crud.create_job(db, job, user.id, "started")
    if files.check_directory(user, upload_folder) is False:
        files.create_user_directory(user, upload_folder)
    files.create_job_directory(user.id, job.id, upload_folder)
    #begin task creation and assignment
    crud.create_tasks(db, job)
    return job

@router.get("/render")
def get_jobs(
        db: Session = Depends(get_db),
        token : str = Depends(auth.oauth2_scheme),
        status: str = None,
        skip: int = 0,
        limit: int = 100,
        ):
    """This function returns a list of all jobs created by a user"""
    user = auth.get_current_client(db, "user", token)
    return crud.get_jobs_by_user(db, user.id, status, skip, limit)

@router.get("/render/{job_id}")
def get_job(
        job_id: int,
        db: Session = Depends(get_db),
        token: str = Depends(auth.oauth2_scheme),
        ):
    """The route for users to check on the status of a job"""
    user = auth.get_current_client(db, "user", token)
    job = crud.get_job(db, job_id)
    check_for_404(job)
    if job.user != user.id:
        return "error"
    return job

@router.put("/render/{job_id}")
def update_job(
        job_id: int,
        job_update: schemas.JobUpdate,
        db: Session = Depends(get_db),
        token: str = Depends(auth.oauth2_scheme),
        ):
    """This is a status for updating jobs already submitted,
    either to increase their frame count, or to cancel"""
    allowed_statuses = {'canceled', None}
    user = auth.get_current_client(db, "user", token)
    job = crud.get_job(db, job_id)
    if job.user != user.id:
        raise HTTPException(status_code=403, detail="No Access to Resource")
    elif job_update.status not in allowed_statuses:
        raise HTTPException(status_code=422, detail="Invalid Status")
    elif job_update.status is not None:
        crud.update_job_status(db, job.id, job_update.status)
        if job_update.status == 'canceled':
            crud.cancel_tasks(db, job.id)
            db.refresh(job)
            return job
    elif job_update.frame_start is not None:
        if job_update.frame_start < job.frame_start:
            crud.increase_job_start(db, job.id, job_update)
        else:
            raise HTTPException(status_code=422, detail="Frame count can't be reduced after job creation")
    elif job_update.frame_end is not None:
        if job_update.frame_end > job.frame_end:
            crud.increase_job_end(db, job.id, job_update)
        else:
            raise HTTPException(status_code=422, detail="Frame count can't be reduced after job creation")
    db.refresh(job)
    return job

@router.get("/render/{job_id}/tasks/")
def get_tasks_for_job(
        job_id: int,
        db: Session = Depends(get_db),
        token: str = Depends(auth.oauth2_scheme),
        task_status: str = None,
        skip: int = 0,
        limit: int = 100,
        ):
    """returns a list of tasks for a specific job"""
    user = auth.get_current_client(db, "user", token)
    db_job = crud.get_job(db, job_id)
    check_for_404(db_job)
    if db_job.user != user.id:
        raise HTTPException(status_code=403, detail="no access to requested resource")
    return crud.get_tasks_by_job_id(db, job_id, task_status, skip, limit)


@router.get("/render/{job_id}/results")
def get_available_results(
        job_id: int,
        db: Session = Depends(get_db),
        token: str = Depends(auth.oauth2_scheme),
        skip: int = 0,
        limit: int = 100,
        ):
    """This route returns a list of completed tasks for download"""
    user = auth.get_current_client(db, "user", token)
    job = crud.get_job(db, job_id)
    check_for_404(job)
    if job.user != user.id:
        return "error"
    return crud.get_results_by_job(db, job.id, skip, limit)

@router.get("/render/{job_id}/{result_id}")
def get_result(
    job_id: int,
    result_id: int,
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
    upload_folder = Depends(get_upload_folder),
    ):
    """This route returns the resulting image file from a specific task"""
    user = auth.get_current_client(db, "user", token)
    job = crud.get_job(db, job_id)
    check_for_404(job)
    if job.user != user.id:
        return "error"
    result = crud.get_result(db, result_id)
    check_for_404(result)
    if job.id != result.job:
        return "error"
    directory = os.path.join(upload_folder, str(job.user), str(job.id), str(result.name))
    return FileResponse(directory, media_type='octet', filename=result.name)

