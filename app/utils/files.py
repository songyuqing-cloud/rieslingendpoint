import os, zipfile, shutil, zlib
from werkzeug.utils import secure_filename

from app.utils import schemas, models 

def create_user_directory(user: schemas.User, upload_folder):
    os.mkdir(os.path.join(upload_folder, str(user.id)))

def get_directory_string(user_id: int, filename: str, upload_folder):
    return os.path.join(upload_folder, user_id, filename)

def check_directory(user: schemas.User, upload_folder):
    return os.path.exists(os.path.join(upload_folder, str(user.id)))

def create_job_directory(user_id: int, job_id: int, upload_folder):
    os.mkdir(os.path.join(upload_folder, str(user_id), str(job_id)))

def save_file(directory: str, fileobj):
    empty_file_obj = open(os.path.join(directory), 'wb')
    shutil.copyfileobj(fileobj, empty_file_obj)
    empty_file_obj.close()

def get_file_size(directory: str):
    return os.path.getsize(directory)

def create_output_file_name(job: models.Job):
    return "job_" + str(job.id) + "frame" + str(job.frame_start) + "-" + str(job.frame_end) + ".zip"

def create_output_file_directory_string(job: models.Job, upload_folder):
    return os.path.join(str(upload_folder), str(job.user), str(job.id))

def compress_finished_job(job: models.Job, upload_folder):
    """This function can take a long time to run, so be careful about where it's invoked.
    It's best if this function is invoked in the background"""
    output_file_name = create_output_file_name(job)
    output_directory_string = create_output_file_directory_string(job, upload_folder) 
    output_file = zipfile.ZipFile(output_directory_string + "/" + output_file_name, 'w', 
        compression=zipfile.ZIP_DEFLATED, allowZip64=True, compresslevel=9,
        )

    for folder, subfolders, files in os.walk(output_directory_string):
        for file in files:
            if str(job.output_file_type).lower() in file:
                output_file.write(filename=os.path.join(str(upload_folder), str(job.user), str(job.id), str(file)), arcname=str(file), compress_type=zipfile.ZIP_DEFLATED)

    output_file.close()

def check_for_compressed_file(job: models.Job, upload_folder):
    output_file_name = create_output_file_name(job)
    output_directory_string = create_output_file_directory_string(job, upload_folder)
    return os.path.exists(output_directory_string + "/" + output_file_name)

def get_compressed_file_directory_string(job: models.Job, upload_folder):
    output_file_name = create_output_file_name(job)
    output_directory_string = create_output_file_directory_string(job, upload_folder)
    return output_directory_string + "/" + output_file_name

def get_upload_folder():
    return os.path.join(os.getcwd() + '/app/files/')

