#standard library packages
import os

#third party packages
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

#local packages
from app.utils import settings

SQLALCHEMY_DATABASE_URL = os.getenv("SQLALCHEMY_DATABASE_URL")

engine = create_engine(
        SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

def get_db():
    try: 
        db = SessionLocal() 
        yield db 
    finally: 
        db.close() 
