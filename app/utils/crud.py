from sqlalchemy.orm import Session, joinedload
from typing import List
from datetime import datetime
import itertools

from app.utils import models, schemas

def create_user(db: Session, user: schemas.UserCreate):
    db_user = models.User(username=user.username, email=user.email, password=user.password, admin=user.admin, active=True)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

def get_user(db: Session, user: schemas.User):
    return db.query(models.User).filter(models.User.username == user.username).first()

def get_user_by_username(db: Session, username: str):
    user = db.query(models.User).filter(models.User.username == username).first()
    return user 

def get_user_by_email(db: Session, email: str):
    user = db.query(models.User).filter(models.User.email == email).first()
    return user

def get_first_user(db: Session):
    return db.query(models.User).first()

def create_node(db: Session, node: schemas.NodeCreate, expiration):
    db_node = models.Node(name=node.name, status=node.status, expiration=expiration)
    db.add(db_node)
    db.commit()
    db.refresh(db_node)
    return db_node

def get_node_by_id(db: Session, id: int):
    return db.query(models.Node).filter(models.Node.id == id).first()

def get_node_by_name(db: Session, name: str):
    return db.query(models.Node).filter(models.Node.name == name).first()

def get_nodes(db: Session, status: str = None, skip: int = 0, limit: int = 100):
    if status == None:
        return db.query(models.Node).offset(skip).limit(limit).all()
    else:
        return db.query(models.Node).filter(models.Node.status == status).offset(skip).limit(limit).all()

def update_node(db: Session, id: int, node: schemas.NodeCreate):
    db_node = db.query(models.Node).filter(models.Node.id == id).first()
    db_node.name = node.name
    db_node.status = node.status
    db.commit()
    db.refresh(db_node)
    return db_node

def deallocate_tasks(db: Session, node_id: int):
    db_tasks = db.query(models.Task).filter(models.Node.id == node_id).filter(models.Task.status != "complete").filter(models.Task.status != "canceled").all()
    for task in db_tasks:
        task.status = "created"
        task.node = None
        db.add(task)
    db.commit()
    return

def get_active_nodes(db: Session, status: str):
    return db.query(models.Node).filter(models.Node.status == status).all()

def get_expired_nodes(db: Session, expires: datetime):
    return db.query(models.Node).filter(models.Node.expiration <= expires).all()

def create_file_entry(db: Session, file: schemas.FileCreate):
    db_file = models.File(name=file.name, size=file.size, user=file.user)
    db.add(db_file)
    db.commit()
    db.refresh(db_file)
    return db_file

def get_file_entry(db: Session, id: int):
    return db.query(models.File).filter(models.File.id == id).first()

def get_files_by_user(db: Session, user: int, skip: int = 0, limit: int = 100):
    return db.query(models.File).filter(models.File.user == user).offset(skip).limit(limit).all()

def get_active_job_files(db: Session):
    return db.query(models.File).filter(models.Job.status != 'complete').all()
     
def create_result(db: Session, job: id, task: id, name: str):
    db_result = models.Result(job=job, task=task, name=name)
    db.add(db_result)
    db.commit()
    db.refresh(db_result)
    return db_result

def get_result(db: Session, result_id: int):
    return db.query(models.Result).filter(models.Result.id == result_id).first()

def get_results_by_job(db: Session, job: int, skip: int = 0, limit: int = 100):
    return db.query(models.Result).filter(models.Result.job == job).offset(skip).limit(limit).all()
    
def get_job(db: Session, id: int):
    return db.query(models.Job).filter(models.Job.id == id).first()

def create_job(db: Session, job: schemas.JobBase, user_id: int, status: str):
    db_job = models.Job(frame_start=job.frame_start, frame_end=job.frame_end,
            samples=job.samples, file_id=job.file_id, output_file_type=job.output_file_type,
            status=status, user=user_id, file=job.file_id, start_time=datetime.utcnow(), end_time=None,
            render_engine=job.render_engine)
    db.add(db_job)
    db.commit()
    db.refresh(db_job)
    return db_job

def increase_job_start(db: Session, job_id: int, job: schemas.JobUpdate):
    db_job = db.query(models.Job).filter(models.Job.id == job_id).first()
    db_job.frame_start = job.frame_start
    db.add(db_job)
    db.commit()
    for frame in range(job.frame_start, db_job.frame_start):
        db_task = models.Task(job=db_job.id, frame=frame,
            output_file_type=db_job.output_file_type, file=db_job.file, node=None, status="created")
        db.add(db_task)
        db.commit()

def increase_job_end(db: Session, job_id: int, job: schemas.JobUpdate):
    db_job = db.query(models.Job).filter(models.Job.id == job_id).first()
    db_job.frame_end = job.frame_end
    db.add(db_job)
    db.commit()
    for frame in range(db_job.frame_end, job.frame_end+1):
        db_task = models.Task(job=db_job.id, frame=frame,
            output_file_type=db_job.output_file_type, file=db_job.file, node=None, status="created")
        db.add(db_task)
        db.commit()

def update_job_status(db: Session, job_id: int, status: str):
    db_job = db.query(models.Job).filter(models.Job.id == job_id).first()
    db_job.status = status
    db.add(db_job)
    db.commit()

def get_jobs_by_user(db: Session, user_id: int, status: str = None, skip: int = 0, limit = 100):
    if status == None:
        return db.query(models.Job).filter(models.Job.user == user_id).offset(skip).limit(limit).all()
    else:
        return db.query(models.Job).filter(models.Job.user == user_id, models.Job.status == status).offset(skip).limit(limit).all()

def get_jobs_by_node(db: Session, node: int, status):
    return db.query(models.Job).filter(models.Job.status == status).filter(models.Task.node == node).all()

def create_tasks(db: Session, job: schemas.Job):
    '''creates a tasks based on a job input and leaves node assignment blank'''
    for frame in range(job.frame_start, job.frame_end+1):
        db_task = models.Task(job = job.id, frame=frame,
                output_file_type=job.output_file_type, file=job.file, node=None, 
                status="created", render_engine=job.render_engine,
                )  
        db.add(db_task)
        db.commit()

def cancel_tasks(db: Session, job_id: int):
    """This function is called after a job is canceled to prevent any remaining tasks from being run.
    It is only used after said job has been canceled"""
    db_tasks = db.query(models.Task).filter(models.Task.job == job_id, models.Task.status != 'complete').all()
    for task in db_tasks:
        task.status = 'canceled'
        task.node = None
        db.add(task)
        db.commit()
    return "successful update"

def get_task(db: Session, id: int):
    return db.query(models.Task).filter(models.Task.id == id).first()

def get_first_incomplete_task_by_job(db: Session, job: int):
    return db.query(models.Task).filter(models.Task.job == job).filter(models.Task.status != 'complete').first()

def get_task_by_files(db: Session, file: list, status: str):
    return db.query(models.Task).filter(models.Job.file_id.in_(file), models.Task.status == status).join(models.File, models.Task.job==models.Job.id).first()

def get_tasks_by_job_id(db: Session, job_id: int, status: str = None, skip: int = 0, limit: int = 100):
    if status == None:
        return db.query(models.Task).filter(models.Task.job == job_id).offset(skip).limit(limit).all()
    else:
        return db.query(models.Task).filter(models.Task.job == job_id).filter(models.Task.status == status).offset(skip).limit(limit).all()

def get_tasks_by_node(db: Session, node: int, status: None, job: None):
    return db.query(models.Task).filter(models.Task.node == node, models.Task.status == status, models.Task.job == job).all()

def get_tasks_for_clean_up(db: Session, node: list):
    return db.query(models.Task).filter(models.Task.node.in_(node), models.Task.status != 'complete').all()

def get_task_log(db: Session, task_id, node_id: int = None):
    if node_id == None:
        return db.query(models.TaskLog).filter(models.TaskLog.task == task_id).all()
    else:
        return db.query(models.TaskLog).filter(models.TaskLog.task == task_id).filter(models.TaskLog.node == node_id).all()

def create_task_log(db: Session, task_id, task_log: str, node_id: int, submit_time):
    db_task_log = models.TaskLog(task = task_id, node = node_id, log = task_log, submit_time = submit_time)
    db.add(db_task_log)
    db.commit()
    return db.refresh(db_task_log)
