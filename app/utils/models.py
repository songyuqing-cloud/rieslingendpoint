#third party packages
from sqlalchemy import DateTime, Boolean, Column, ForeignKey, Float, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class User(Base):
    '''table for storing user information, referenced in almost everything else'''
    __tablename__ = "user"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    email = Column(String, unique=True, index=True)
    active = Column(Boolean)
    admin = Column(Boolean)
    password = Column(String)
    created_by = Column(Integer, ForeignKey('user.id'))
    created_time = Column(DateTime)

class Job(Base):
    '''highest level representation of work to be done. Broken out into individual tasks'''
    __tablename__ = "job"

    id = Column(Integer, primary_key=True, index=True)
    frame_start = Column(Integer)
    frame_end = Column(Integer)
    samples = Column(Integer)
    file_id = Column(String)
    output_file_type = Column(String)
    status = Column(String)
    user = Column(Integer, ForeignKey("user.id")) 
    file = Column(Integer, ForeignKey("file.id"))
    start_time = Column(DateTime)
    end_time = Column(DateTime)
    render_engine = Column(String)

class Task(Base):
    '''executable level of work, currently a frame, but could also be a chunk in future iterations'''
    __tablename__ = "task"

    id = Column(Integer, primary_key=True, index=True)
    job = Column(Integer, ForeignKey('job.id'))
    frame = Column(Integer)
    output_file_type = Column(String, ForeignKey('job.output_file_type'))
    node = Column(String, ForeignKey('node.id'))
    file = Column(String, ForeignKey('job.id'))
    status = Column(String)
    start_time = Column(DateTime)
    end_time = Column(DateTime)
    render_engine = Column(String, ForeignKey('job.render_engine'))

class TaskLog(Base):
    '''logs submitted by a node about how a task was executed'''
    __tablename__ = "task_log"
    
    id = Column(Integer, primary_key=True, index=True)
    task = Column(Integer, ForeignKey('task.id'))
    node = Column(Integer, ForeignKey('node.id'))
    log = Column(String)
    submit_time = Column(DateTime)

class Node(Base):
    '''a computer/container/virtual machine that executes tasks and returns a result to contribute to finishing a job'''
    __tablename__ = "node"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(Integer, index=True)
    status = Column(String)
    expiration = Column(DateTime)

class File(Base):
    '''the source file included in a job in .blend format'''
    __tablename__ = "file"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    size = Column(Integer)
    user = Column(Integer, ForeignKey("user.id"))

class Result(Base):
    '''the result of a finished task'''
    __tablename__ = "result"

    id = Column(Integer, primary_key=True, index=True)
    job = Column(Integer, ForeignKey("job.id"))
    task = Column(Integer, ForeignKey("task.id"))
    name = Column(String) 
