from fastapi import HTTPException

def check_for_404(input):
    if input is None:
        raise HTTPException(status_code=404, detail="Resource not found")
