#standard library
from typing import List, Optional
from datetime import datetime

#local packages
from pydantic import BaseModel

class UserBase(BaseModel):
    '''as with all Base schemas, this defines the request body for creating
    an object of this sort'''
    username: str
    email: str
    admin: bool
    
class UserCreate(UserBase):
    '''keeps everyting from UserBase, and includes a password field
    so that not all request or response bodies would need to include it'''
    password: str

class User(UserBase):
    '''everything in a user except the password. Used for request/response
    bodies after user creation'''
    id: int
    
    class Config:
        orm_mode = True

class FileCreate(BaseModel):
    '''submission information for creating file entries in the database'''
    name: str
    size: int 
    #in bytes
    user: int

class File(FileCreate):
    '''request/response body used after file creation'''
    id: int

    class Config:
        orm_mode = True

class Result(BaseModel):
    '''The result of a Task's execution'''
    id: int
    job: int
    name: str

    class Config:
        orm_mode = True

class JobBase(BaseModel):
    '''request body schema for job creation'''
    frame_start: int 
    frame_end: int 
    samples: int
    file_id: int 
    output_file_type: str = "PNG"
    render_engine: str = "CYCLES"

class Job(JobBase):
    '''includes everything from JobBase, and adds the id for request/responses'''
    id: int
    status: str

    class Config:
        orm_mode = True

class JobUpdate(BaseModel):
	'''body used for job updates from a user'''
	status: Optional[str] = None
	frame_start: Optional[int] = None
	frame_end: Optional[int] = None

class TaskBase(BaseModel):
    '''schema for information needed to create a task object'''
    job: int
    frame: int
    output_file_type: str
    node: int
    status: str

class Task(TaskBase):
    '''task schema used after database creation'''
    id: int

    class Config:
        orm_mode = True

class TaskUpdate(BaseModel):
    status: str

class TaskLogCreate(BaseModel):
    log: str

class TaskLog(TaskLogCreate):
    id: int
    node: int
    submit_time: str

class NodeCreate(BaseModel):
    name: str
    status: str = "created"

class Node(NodeCreate):
    id: int
    expiration: datetime

    class Config:
        orm_mode = True
