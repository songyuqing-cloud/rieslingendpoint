#standard library
import os
from datetime import datetime, timedelta

#third party
import jwt
from jwt import PyJWTError
from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from pydantic import BaseModel, ValidationError
from starlette.status import HTTP_401_UNAUTHORIZED
from passlib.context import CryptContext

#local packages
from app.utils import crud, models, schemas, database, settings

SECRET_KEY = os.getenv("SECRET_KEY")

ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 20000

password_context = CryptContext(schemes=['bcrypt'], deprecated='auto')

oauth2_scheme = OAuth2PasswordBearer(tokenUrl = "/auth/token")

class Token(BaseModel):
    access_token: str
    token_type: str

class TokenData(BaseModel):
    client: str = None

def get_password_hash(password):
    return password_context.hash(password)

def verify_password_with_hash(password, hashed_password):
    return password_context.verify(password, hashed_password)

def authenticate_user_password(db: Session, username: str, password: str):
    user = crud.get_user_by_username(db, username)
    if not user:
        return False
    if not verify_password_with_hash(password, user.password):
        return False
    return user

def create_access_token(*, data: dict, expires_delta: timedelta = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=30)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt

def get_current_client(db: Session, token_type: str, token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="could not validate credentials",
            headers={"WWW-Authenticate":"Bearer"},
            )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        token_type ==  payload.get("token_type")
        if token_type == "user":
            username: str = payload.get("sub")
            if username is None:
                raise credentials_exception
            token_data = TokenData(client=username)
        elif token_type == "node":
            name: str = payload.get("sub")
            if name is None:
                raise credentials_exception
            token_data = TokenData(client=name)
    except (PyJWTError):
        print("error from JWT decoding")
        raise credentials_exception
    if token_type == "user":
        user = crud.get_user_by_username(db, username=token_data.client)
        if user is None:
            raise credentials_exception
        return user
    else:
        node = crud.get_node_by_name(db, name=token_data.client)
        if node is None:
            raise credentials_exception
        return node
