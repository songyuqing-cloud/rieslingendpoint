#standard library
import json
import os
import tempfile

#third party packages
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

#local modules
from ..main import app
from ..utils.models import Base
from ..utils.database import get_db
from ..utils.files import get_upload_folder

### begin sql testing magic ###
SQLALCHEMY_DATABASE_URL = 'sqlite:///./sqltest.db'

engine = create_engine(
        SQLALCHEMY_DATABASE_URL, connect_args={'check_same_thread': False}
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()

def override_get_upload_folder():
    upload_directory = tempfile.mkdtemp()
    return upload_directory 

app.dependency_overrides[get_db] = override_get_db
app.dependency_overrides[get_upload_folder] = override_get_upload_folder

#make sure previous databases go away
if os.path.isfile("sqltest.db"):
    os.remove("sqltest.db")
else:
    pass

### end sql testing magic ###

### begin file testing clean up ###
if os.path.isfile("test.db"):
    os.remove("test.db")
else:
    pass

if os.path.isfile("1.blend"):
    os.remove("1.blend")
else:
    pass
### end file testing clean up ###

#create a testing instance of the app that we can connect to
client = TestClient(app)

Base.metadata.create_all(bind=engine)

#later filled in during the login route
token = None
headers = None
node_token = None

def test_server_response():
    '''this ensures that the test server started correctly (imports)
    and is responding to requests'''
    response = client.get("/docs")
    assert response.status_code == 200 

def test_create_user():
    '''tests that the server allows users to be created in an empty
    database and that the checks in place for that work'''
    payload = {
        "username": "test",
        "email": "test@example.com",
        "admin": True,
        "password": "test"
        }
    response = client.post("/auth/create", data=json.dumps(payload))
    assert response.status_code == 200

def test_login():
    '''after a user is created, tests that logging in and obtaining
    a token works'''
    payload = {'username': 'test', 'password': 'test'}
    response = client.post("/auth/token", data=payload)
    response_json = json.loads(str(response.text))
    global token
    print("Token should be None: " + str(token))
    token = response_json['access_token']
    print("Should have something now: " + token)
    print(response)
    assert response.status_code == 200

def test_token():
    '''submits the OAuth token to make sure it's valid''' 
    global token
    global headers 
    headers = {'Authorization': 'Bearer ' + token}
    response = client.post("/auth/test", headers=headers)
    print(response)
    assert response.status_code == 200

def test_create_node():
    '''tests that nodes can be created'''
    global headers
    global node_token
    payload = {"name": "test_node", "status": "created"}
    response = client.post("/node/", headers=headers, data=json.dumps(payload))
    print(response.text)
    response_json = json.loads(str(response.text))
    node_token = response_json[1]
    assert response.status_code == 200
    #base64 encoded beginning of every jwt
    assert "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9" in node_token
