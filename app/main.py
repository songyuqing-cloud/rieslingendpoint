#standard library packages
import os
import shutil
import random
from datetime import datetime, timedelta
from typing import List

#fastAPI related packages
from fastapi import (FastAPI, Depends,
                    File, UploadFile,
                    HTTPException, Query, Header,
                    Request)
from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from starlette.responses import FileResponse
from starlette.status import (HTTP_401_UNAUTHORIZED,
                                HTTP_404_NOT_FOUND,
                                HTTP_403_FORBIDDEN)
#local packages
from app.utils import models
from app.utils.database import engine
from app.routers import admin, nodes, files, status, auth_router, jobs

models.Base.metadata.create_all(bind=engine)

app = FastAPI()
app.include_router(auth_router.router)
app.include_router(files.router)
app.include_router(jobs.router)
app.include_router(nodes.router)
app.include_router(status.router)
app.include_router(admin.router)


templates = Jinja2Templates(directory="app/templates")
app.mount("/static", StaticFiles(directory="app/static"), name="static")

@app.on_event("startup")
def delete_test_database():
    test_db = os.path.join(os.getcwd() + '/app/tests/sqltest.db')
    if os.path.isfile(test_db):
        os.remove(test_db)
        print("removed test database") 
    else:
        pass

@app.get("/")
def index(request: Request):
    """returns the front end built in javascript"""
    return templates.TemplateResponse("index.html", {"request": request})

@app.get("/license")
def return_license():
    """This returns a copy of the license that the software is released under"""
    #maybe find a way of speeding this up
    return FileResponse("LICENSE")
