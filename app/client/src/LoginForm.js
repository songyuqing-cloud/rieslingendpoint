import React, {Component} from 'react'
import {saveToken} from './authHelper'

class LoginForm extends Component {
    constructor(props) {
	super(props)
	this.state = {username: '', password: '',}
    }

	handleSubmit = async (event) => {
	    let formData = new FormData()
	    formData.append("username", this.state.username)
	    formData.append("password", this.state.password)
	    const response = await fetch("/auth/token", {method: "POST", body: formData})
	    //verify response status code
	    if(response.status === 200){
		saveToken(await response.json())
		this.props.onLoggedIn()
	    }
	}

	handleChange = (event) => {
	    const {name, value} = event.target
		
	    this.setState({
			[name]: value,
	    })
	}
	render () {
	    return (
		<div className="vertical-center">
		<div className="large-container">
		<div className="login">
		    <form action="/auth/token" method="post">
			<label htmlFor="username">Username</label>
			    <input
				type="text"
			    	    name="username"
			    	    id="username"
				    onChange={this.handleChange} />
			    <label htmlFor="password">Password</label>
				<input
			   	    type="password"
			   	    name="password"
				    id="password"
				    onChange={this.handleChange} />
				<input
				    type="button"
				    value="Submit"
				    onClick={this.handleSubmit} />
		    </form>
		</div>
		</div>
		</div>
	    )};
}

export default LoginForm
