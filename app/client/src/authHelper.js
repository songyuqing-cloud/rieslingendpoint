export const ACCESS_TOKEN = 'Authorization'
//the key used for localStorage() 

export function saveToken(input) {
	const token = "Bearer " + input.access_token
	//localStorage.setItem(ACCESS_TOKEN, token)
	document.cookie = `${ACCESS_TOKEN}=${token}`
}

export function getToken() {
    return document.cookie.split('; ').find(row => row.startsWith(ACCESS_TOKEN)).split('=')[1];
}

export function authHeaders() {
	let headers = new Headers()
	const access_token = getToken();
	headers.append('Authorization', access_token)
	return headers
}

