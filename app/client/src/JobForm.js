import React, {Component} from 'react'
import {authHeaders} from './authHelper'
import handleChange from './handleChange'
import FileOption from './fileOption'

class JobForm extends Component {
    constructor(props) {
	super(props);
	this.state = {job: {
			frame_start: null,
			frame_end: null,
			samples: null,
			file_id: null,
			output_file_type: '',}
	};
	this.files = null
    }

    handleChange = handleChange.bind(this) 

    GetFiles = async () => {
        const response = await fetch("/files/", { headers: authHeaders() })
        const files = await response.json() 
        const fileOptions = files.map(file => <option key={file.id} id={file.id}>{file.name}</option>);
        console.log(fileOptions[0])
        return (<div>{fileOptions}</div>);
    }

    BuildOptions = () => {
        const files = this.files
        const fileOptions = files.map(file => <option key={file.id} id={file.id}>{file.name}</option>);
        console.log(fileOptions[0])
        return {fileOptions}
    }

    render() {
	return ( 
	    <form onSubmit={this.handleSubmit}>
		<label>Select the file for this job</label>
		<select name="file_id" id="file_id">
		    <this.GetFiles />
		</select>
		<label>Starting Frame</label>
		    <input type="number"
			name="frame_start"
			id="frame_start"
			onChange={this.handleChange} />
		<label>Ending Frame</label>
		    <input type="number"
			name="frame_end"
			id="frame_end"
			onChange={this.handleChange} />
		<label>Samples</label>
		    <input type="number"
			name="samples"
			id="samples"
			onChange={this.handleChange} />
		<label>Output File Type</label>
		    <input type="text"
			name="output_file_type"
			id="output_file_type"
			onChange={this.handleChange} />
		<button type="submit">Submit</button>
	    </form>
	)
    }
}

export default JobForm
