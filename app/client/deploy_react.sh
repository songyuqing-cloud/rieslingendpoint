#!/bin/bash

# This script is run after npm run-script build to copy
# the contents of the build folder to the directories used by
# the fastAPI app itself
cp build/index.html ../templates/index.html && cp build/favicon.png ../static/favicon.png && cp -r build/static/* ../static

echo Copied successfully!
